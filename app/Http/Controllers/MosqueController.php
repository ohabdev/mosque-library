<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mosque;

class MosqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mosques = Mosque::orderBy('id', 'desc')->get();
        return view('home', ['mosques' => $mosques]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mosque_add_from');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $mosque = new Mosque;

        $mosque->li_type = $request->li_type;
        $mosque->mos_li_name = $request->mos_li_name;
        $mosque->division = $request->division;
        $mosque->district = $request->district;
        $mosque->upazilla = $request->upazilla;
        $mosque->union = $request->union;
        $mosque->village = $request->village;
        $mosque->li_o_year = $request->li_o_year;
        $mosque->i_f_p_bn = $request->i_f_p_bn;
        $mosque->b_p_p_bn = $request->b_p_p_bn;
        $mosque->i_k_p = $request->i_k_p;
        $mosque->n_o_p = $request->n_o_p;
        $mosque->amlamir_date = $request->amlamir_d .'-'. $request->amlamir_m .'-'. $request->amlamir_y;
        $mosque->total_e_p_no = $request->total_e_p_no;
        $mosque->sineboard_is = $request->sineboard_is;
        $mosque->ms_e_p = $request->ms_e_p;
        $mosque->month_avg_p = $request->month_avg_p;
        $mosque->p_no_l = $request->p_no_l;
        $mosque->monthly_female_p = $request->monthly_female_p;

        $mosque->ms_activities = $request->ms_activities_b_s .'-'. $request->ms_activities_s_o .'-'.$request->ms_activities_o_m .'-'.$request->ms_activitie_s_s;

        $mosque->l_v_date = $request->last_visit_date .'-'. $request->last_visit_month .'-'. $request->last_visit_year; 

        $mosque->l_v_p_name = $request->lasl_vstd_person_name;
        $mosque->l_v_p_address = $request->lasl_vstd_person_address;

        $mosque->save();
        return redirect()->back()->with('success', 'Success! Mosque Library information successfully saved.');

// array:31 [▼
//   "_token" => "ocsnhm9S0JVpn06nlc4APJ8pgoyIYfp8jTlwgdXR"
//   "li_type" => "মডেল মসজিদ পাঠাগার"
//   "mos_li_name" => "dsafas"
//   "division" => "dsaf"
//   "district" => "asdfadf"
//   "upazilla" => "sadfas"
//   "union" => "sdafsd"
//   "village" => "dsafas"
//   "li_o_year" => "4353"
//   "i_f_p_bn" => "3453"
//   "b_p_p_bn" => "345"
//   "i_k_p" => "345"
//   "n_o_p" => "345"
//   "amlamir_d" => "45"
//   "amlamir_m" => "345"
//   "amlamir_y" => "345"
//   "total_e_p_no" => null
//   "sineboard_is" => "হ্যাঁ"
//   "ms_e_p" => "34"
//   "month_avg_p" => "345"
//   "p_no_l" => "তরুণ/তরুণী"
//   "monthly_female_p" => "345"
//   "ms_activities_b_s" => "বয়স্ক শিক্ষা"
//   "ms_activities_s_o" => "সাংস্কৃতিক  অনুষ্ঠান"
//   "ms_activities_o_m" => "ওয়াজ মাহফিল"
//   "ms_activitie_s_s" => "সমসাময়িক সামাজিক ইস্যুভিত্তিক আলোচনা"
//   "last_visit_date" => "454"
//   "last_visit_month" => "454"
//   "last_visit_year" => "454"
//   "lasl_vstd_person_name" => "asdfas"
//   "lasl_vstd_person_address" => "saf"
// ]
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
