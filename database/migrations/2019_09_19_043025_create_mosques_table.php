<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMosquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosques', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('li_type')->nullable();
            $table->string('mos_li_name')->nullable();
            $table->string('division')->nullable();
            $table->string('district')->nullable();
            $table->string('upazilla')->nullable();
            $table->string('union')->nullable();
            $table->string('village')->nullable();
            $table->string('li_o_year')->nullable();
            $table->string('i_f_p_bn')->nullable();
            $table->string('b_p_p_bn')->nullable();
            $table->string('i_k_p')->nullable();
            $table->string('n_o_p')->nullable();
            $table->string('amlamir_date')->nullable();
            $table->string('total_e_p_no')->nullable();
            $table->string('sineboard_is')->nullable();
            $table->string('ms_e_p')->nullable();
            $table->string('month_avg_p')->nullable();
            $table->string('p_no_l')->nullable();
            $table->string('monthly_female_p')->nullable();
            $table->string('ms_activities')->nullable();
            $table->string('l_v_date')->nullable();
            $table->string('l_v_p_name')->nullable();
            $table->string('l_v_p_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosques');
    }
}
