@extends('layouts.app')

@section('content')
<div class="container-fluid">
    @if (session('success'))
        <div class="alert alert-success success-alert">
            {{ session('success') }}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center">
                    <a href="{{ route('mosque.index') }}" class="btn btn-sm btn-success" style="float: left;"> < Back Home </a>
                    <span  class="navbar-brand">মসজিদ পাঠাগার ডাটাবেজ ফরম</span> 
                    <a href="{{ route('mosque.index') }}" class="btn btn-md btn-primary" style="float: right;"><span class="glyphicon glyphicon-th-list"></span> View List </a>
                </div>
                <form action="{{ route('mosque.store') }}" method="POST">
                @csrf
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row mb-2">
                            <div class="col-md-1">১.</div>
                            <div class="col-md-3">পাঠাগার ধরণ</div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-1"><input type="radio" name="li_type" value="মডেল মসজিদ পাঠাগার"></div>
                                    <div class="col-md-3">মডেল মসজিদ পাঠাগার</div>
                                    <div class="col-md-1"><input type="radio" name="li_type" value="সাধারণ মসজিদ পাঠাগার"></div>
                                    <div class="col-md-3"> সাধারণ মসজিদ পাঠাগার</div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">২.</div>
                            <div class="col-md-3">মসজিদ পাঠাগারের নাম</div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="mos_li_name" required>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">৩.</div>
                            <div class="col-md-3">বিভাগ </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="division">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">৪.</div>
                            <div class="col-md-3">জেলা </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="district">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">৫.</div>
                            <div class="col-md-3"> উপজেলা/পৌরসভা </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="upazilla">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">৬.</div>
                            <div class="col-md-3"> ইউনিয়ন </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="union">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">৭.</div>
                            <div class="col-md-3"> গ্রামের নাম/মহল্লার নাম ও রোড  নং </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <textarea class="form-control" name="village"></textarea>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">৮.</div>
                            <div class="col-md-3"> পাঠাগার প্রতিষ্ঠার অর্থ বছর </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-4">
                                 <input type="text" class="form-control" name="li_o_year">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">৯.</div>
                            <div class="col-md-3"> ইসলামিক ফাউন্ডেশন প্রদত্ত পুস্তকের সংখ্যা </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-4">
                                 <input type="text" class="form-control" name="i_f_p_bn">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">১০.</div>
                            <div class="col-md-3"> বাইরের প্রকাশনার প্রদত্ত পুস্তকের সংখ্যা </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="b_p_p_bn">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-1">১১.</div>
                            <div class="col-md-3"> পুস্তক সংরক্ষণ জন্য প্রদত্ত  আমলাবীর সংখ্যা </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">ইফা কর্তৃক প্রদত্ত</span>
                                            </div>
                                                <input type="text" class="form-control" name="i_k_p">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">টি</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">নিজন্ব উদ্যোগে প্রদত্ত</span>
                                            </div>
                                                <input type="text" class="form-control" name="n_o_p">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">টি</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">১২.</div>
                            <div class="col-md-3"> আলমারীর প্রদানের তারিখ </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label >দিন</label>
                                        <input type="text" class="form-control" name="amlamir_d">
                                    </div>
                                    <div class="col-md-3">
                                        <label>মাস</label>
                                        <input type="text" class="form-control" name="amlamir_m">
                                    </div>
                                    <div class="col-md-3">
                                        <label>সন</label>
                                        <input type="text" class="form-control" name="amlamir_y">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">১৩.</div>
                            <div class="col-md-3"> বিগত ১ বছর কর্মকর্তা/কর্মচারী কর্তৃক পরিদর্শন সংখ্যা </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="total_e_p_no">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">১৪.</div>
                            <div class="col-md-3">সাইনবোর্ড স্থাপন করা হয়েছে কিনা ? </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">হ্যাঁ</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <input type="radio" name="sineboard_is" value="হ্যাঁ">
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-1">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">না</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <input type="radio" name="sineboard_is" value="না">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">১৫.</div>
                            <div class="col-md-3">মসজিদ পাঠাগার এর প্রতিদিন পাঠকের সংখ্যা</div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="ms_e_p">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">জন</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">১৬.</div>
                            <div class="col-md-3">মাসিক গড় পাঠক সংখ্যা</div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="month_avg_p">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">জন</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">১৭.</div>
                            <div class="col-md-3">কোন বয়সের পাঠক সংখ্যা বেশি</div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-1">
                                        <input type="radio" name="p_no_l" value="কিশোর/কিশোরী">
                                    </div>
                                    <div class="col-md-3">
                                        কিশোর/কিশোরী 
                                        <pre>(৮-১৮ বছর)</pre>
                                    </div>
                                    <div class="col-md-1">
                                        <input type="radio" name="p_no_l" value="তরুণ/তরুণী">
                                    </div>
                                    <div class="col-md-3">
                                        তরুণ/তরুণী 
                                        <pre>(১৯-৪০ বছর)</pre>
                                    </div>
                                    <div class="col-md-1">
                                        <input type="radio" name="p_no_l" value="বয়স্ক">
                                    </div>
                                    <div class="col-md-3">
                                        বয়স্ক 
                                        <pre>(৪০ বা তদুর্দ্ধ বছর )</pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">১৮.</div>
                            <div class="col-md-3">মাসিক মহিলা পাঠকের গড় সংখ্যা</div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="monthly_female_p">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">জন</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">১৯.</div>
                            <div class="col-md-3">মসজিদ পাঠাগার কেন্দ্রিক পরিচালিত কার্যক্রম</div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <div class="row mt-2">
                                    <div class="col-md-2">
                                        (ক) বয়স্ক শিক্ষা 
                                    </div>
                                    <div class="col-md-1">
                                        <input type="checkbox" name="ms_activities_b_s" value="বয়স্ক শিক্ষা">
                                    </div>
                                    <div class="col-md-3">
                                        (খ) সাংস্কৃতিক  অনুষ্ঠান 
                                    </div>
                                    <div class="col-md-1">
                                        <input type="checkbox" name="ms_activities_s_o" value="সাংস্কৃতিক  অনুষ্ঠান">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-3">
                                        (গ) ওয়াজ মাহফিল
                                    </div>
                                    <div class="col-md-1">
                                        <input type="checkbox" name="ms_activities_o_m" value="ওয়াজ মাহফিল">
                                    </div>
                                    <div class="col-md-6">
                                        (ঘ) সমসাময়িক সামাজিক ইস্যুভিত্তিক আলোচনা 
                                    </div>
                                    <div class="col-md-1">
                                        <input type="checkbox" name="ms_activitie_s_s" value="সমসাময়িক সামাজিক ইস্যুভিত্তিক আলোচনা">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">২০.</div>
                            <div class="col-md-3">পাঠাগার পরিদর্শনের সর্বশেষ তারিখ </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                 <div class="row">
                                    <div class="col-md-2">
                                        <label >দিন</label>
                                        <input type="text" class="form-control"  name="last_visit_date">
                                    </div>
                                    <div class="col-md-2">
                                        <label>মাস</label>
                                        <input type="text" class="form-control" name="last_visit_month">
                                    </div>
                                    <div class="col-md-2">
                                        <label>সন</label>
                                        <input type="text" class="form-control" name="last_visit_year">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">২১.</div>
                            <div class="col-md-3">সর্বশেষ পরিদর্শনকারীর  নাম  </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="lasl_vstd_person_name">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1">২২.</div>
                            <div class="col-md-3">সর্বশেষ পরিদর্শনকারীর পদবী  ও ঠিকানা </div>
                            <div class="col-md-1"> : </div>
                            <div class="col-md-7">
                                <textarea class="form-control" name="lasl_vstd_person_address"></textarea>
                            </div>
                        </div>
                    </div>
                    @if (session('success'))
                        <div class="alert alert-success success-alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="card-footer text-center">
                        <a href="{{ route('mosque.index') }}" class="btn btn-lg btn-danger"> BACK </a>
                        <button class="btn btn-lg btn-success" type="submit">SAVE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".success-alert").fadeTo(5000, 500).slideUp(500, function(){
            $(".success-alert").slideUp(500);
        });
    })
</script>
@endsection 
