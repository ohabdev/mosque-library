@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span  class="navbar-brand text-bold">মসজিদ পাঠাগার সম্প্রসারণ ও শক্তিশালী প্রকল্প</span>
                    <a href="{{ route('mosque.create') }}" class="btn btn-sm btn-success" style="float: right;"> + মসজিদ পাঠাগার ডাটাবেজ ফরম</a>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div style="overflow: scroll; height:100%" class="table-wrapper-scroll-y my-custom-scrollbar"> 
                        <table id="dataTable" class="table table-striped table-bordered table-sm " cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>নাম</th>
                                    <th>ধরন</th>
                                    <th>বিভাগ</th>
                                    <th>জেলা</th>
                                    <th>উপজেলা/পৌর</th>
                                    <th>ইউনিয়ন</th>
                                    <th>গ্রাম-রোড</th>
                                    <th>প্রতিষ্ঠা</th>
                                    <th>পুস্তক সংখ্যা (ইফা)</th>
                                    <th>পুস্তক সংখ্যা (বাহির)</th>
                                    <th>আলমারীর সংখ্যা</th>
                                    <th>আলমারী প্রদানের তারিখ</th>
                                    <th>পরিদর্শন সংখ্যা</th>
                                    <th>সাইনবোর্ড</th>
                                    <th>গড় পাঠক (দৈনিক)</th>
                                    <th>গড় পাঠক (মাসিক)</th>
                                    <th>বেশীরভাগ পাঠকের বয়স</th>
                                    <th style="font-size:11px;">গড় মহিলা পাঠক (মাসিক)</th>
                                    <th>পরিচালিত কার্যক্রম</th>
                                    <th>সর্বশেষ পরিদর্শন</th>
                                    <th>পরিদর্শনকারীর নাম</th>
                                    <th>পরিদর্শনকারীর পদবী ও ঠিকানা</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($mosques as $key=>$mosque)
                                    <tr>
                                      <td>{{ ++$key }}</td>
                                      <td style="font-size:10px;">{{ $mosque->li_type }}</td>
                                      <td style="font-size:10px;">{{ $mosque->mos_li_name }}</td>
                                      <td>{{ $mosque->division }}</td>
                                      <td>{{ $mosque->district }}</td>
                                      <td>{{ $mosque->upazilla }}</td>
                                      <td>{{ $mosque->union }}</td>
                                      <td style="font-size:9px;">{{ $mosque->village }}</td>
                                      <td>{{ $mosque->li_o_year }}</td>
                                      <td>{{ $mosque->i_f_p_bn }}</td>
                                      <td>{{ $mosque->b_p_p_bn }}</td>
                                      <td style="font-size:9px;"> ইফা কর্তৃক {{ $mosque->i_k_p }} টি & নিজন্ব উদ্যোগে {{ $mosque->n_o_p }} টি</td>
                                      <td>{{ $mosque->amlamir_date }}</td>
                                      <td>{{ $mosque->total_e_p_no }}</td>
                                      <td>{{ $mosque->sineboard_is }}</td>
                                      <td>{{ $mosque->ms_e_p }}</td>
                                      <td>{{ $mosque->month_avg_p }}</td>
                                      <td>{{ $mosque->p_no_l }}</td>
                                      <td>{{ $mosque->monthly_female_p }}</td>
                                      <td style="font-size:9px;">{{ $mosque->ms_activities }}</td>
                                      <td>{{ $mosque->l_v_date }}</td>
                                      <td>{{ $mosque->l_v_p_name }}</td>
                                      <td style="font-size:9px;">{{ $mosque->l_v_p_address }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function () {
        $('#dataTable').DataTable();
    });
</script>
@endsection

